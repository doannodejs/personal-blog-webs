const express = require('express');
const router = express.Router();
const Post = require('../models/Post');
const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const adminLayout = '../views/layouts/admin';
const jwtSecret = process.env.JWT_SECRET;

// Middleware kiểm tra xem người dùng có đăng nhập và có vai trò là admin hay không
const authMiddleware = async (req, res, next) => {
  const token = req.cookies.token;
  if (!token) {
    return res.status(401).render('error', { message: 'Không được phép truy cập' });
  }
  try {
    const decoded = jwt.verify(token, jwtSecret);
    req.userId = decoded.userId;
    // Kiểm tra xem người dùng có phải là admin hay không
    const user = await User.findById(decoded.userId);
    if (!user || user.role !== 'admin') {
      // Nếu không phải admin, điều hướng đến trang chính
      return res.status(403).render('error', { message: 'Cấm truy cập' });
    }
    next();
  } catch (error) {
    res.status(401).render('error', { message: 'Không được phép truy cập' });
  }
}

// Trang đăng ký
router.get('/register', async (req, res) => {
  try {
    const locals = {
      title: "Đăng ký",
      description: "Đăng ký để tiếp tục"
    }
    res.render('register', { locals });
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});

// Đăng ký người dùng mới
router.post('/register', async (req, res) => {
  try {
    const { name, username, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10); // Mã hóa mật khẩu
    try {
      await User.create({ name, username, password: hashedPassword });
      res.status(201).render('success', { message: 'Đăng ký thành công' });
    } catch (error) {
      if (error.code === 11000) {
        res.status(409).render('error', { message: 'Tên người dùng đã được sử dụng' });
      }
      res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
    }
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});


// Trang đăng nhập
router.get('/login', async (req, res) => {
  try {
    const locals = {
      title: "Đăng nhập",
      description: "Đăng nhập để tiếp tục"
    }
    res.render('login', { locals });
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});

// Xác thực người dùng đăng nhập
router.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(401).render('error', { message: 'Thông tin đăng nhập không hợp lệ' });
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).render('error', { message: 'Thông tin đăng nhập không hợp lệ' });
    }
    // Kiểm tra nếu người dùng là admin
    if (user.role === 'admin') {
      const token = jwt.sign({ userId: user._id }, jwtSecret);
      res.cookie('token', token, { httpOnly: true });
      res.redirect('/admin');
    } else {
      // Nếu không phải admin, điều hướng đến trang chính
      res.redirect('/');
    }
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});
router.get('/manage-users', authMiddleware, async (req, res) => {
  try {
    const users = await User.find({ role: 'user' }); // Lấy danh sách người dùng có vai trò là 'user'
    const posts = await Post.find(); // Lấy dữ liệu bài viết

    const locals = {
      title: 'Bảng điều khiển',
      description: 'Chia sẻ và học tập cùng Node.js'
    };

    res.render('admin/manage-users', {
      locals,
      users,
      posts,
      layout: adminLayout
    });
  } catch (error) {
    console.error(error);
    res.status(500).render('error', { message: 'Lỗi server khi lấy danh sách người dùng' });
  }
});

  router.post('/edit-user/:id', authMiddleware, async (req, res) => {
    try {
      const { name, username, password } = req.body;
      const hashedPassword = await bcrypt.hash(password, 10); // Mã hóa mật khẩu mới
      const user = await User.findById(req.params.id);
      if (!user) {
        return res.status(404).send('Người dùng không tồn tại');
      }
      user.name = name;
      user.username = username;
      user.password = hashedPassword; // Lưu mật khẩu đã được mã hóa
      await user.save();
      res.redirect('/manage-users');
    } catch (error) {
      console.error(error);
      res.status(500).render('error', { message: 'Lỗi server khi cập nhật thông tin người dùng' });
    }
  });
  

// Đăng xuất người dùng
router.get('/logout', (req, res) => {
  res.clearCookie('token');
  res.redirect('/');
});

// Trang Dashboard của admin
router.get('/admin', authMiddleware, async (req, res) => {
  try {
    const locals = {
      title: 'Bảng điều khiển',
      description: 'Chia sẻ và học tập cùng Node.js'
    }
    const data = await Post.find();
    res.render('admin/dashboard', {
      locals,
      data,
      layout: adminLayout
    });
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});

// Thêm bài viết mới
router.route('/add-post')
  .get(authMiddleware, async (req, res) => {
    try {
      const locals = {
        title: 'Thêm bài viết',
        description: 'Chia sẻ và học tập cùng Node.js'
      };
      const data = await Post.find();
      res.render('admin/add-post', {
        locals,
        layout: adminLayout
      });
    } catch (error) {
      res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
    }
  })
  .post(authMiddleware, async (req, res) => {
    try {
      const newPost = new Post({
        title: req.body.title,
        body: req.body.body
      });

      await Post.create(newPost);
      res.redirect('/admin');
    } catch (error) {
      res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
    }
  });


// Chỉnh sửa bài viết
router.route('/edit-post/:id')
  .get(authMiddleware, async (req, res) => {
    try {
      const locals = {
        title: "Chỉnh sửa bài viết",
        description: "Chia sẻ và học tập cùng Node.js",
      };
      const data = await Post.findOne({ _id: req.params.id });
      res.render('admin/edit-post', {
        locals,
        data,
        layout: adminLayout
      });
    } catch (error) {
      res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
    }
  })
  .put(authMiddleware, async (req, res) => {
    try {
      await Post.findByIdAndUpdate(req.params.id, {
        title: req.body.title,
        body: req.body.body,
        updatedAt: Date.now()
      });
      res.redirect(`/admin/${req.params.id}`);
    } catch (error) {
      res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
    }
  });

// Xóa bài viết
router.delete('/delete-post/:id', authMiddleware, async (req, res) => {
  try {
    await Post.deleteOne({ _id: req.params.id });
    res.redirect('/admin');
  } catch (error) {
    res.status(500).render('error', { message: 'Lỗi máy chủ nội bộ' });
  }
});

module.exports = router;
